#!/usr/bin/env bash
mvn clean package -DskipTests
docker build . -t scratchpad:latest
docker run \
  -e POSTGRES_ENV_POSTGRES_PASSWORD='foo' \
  -e postgres_env_username='bar' \
  scratchpad a b c