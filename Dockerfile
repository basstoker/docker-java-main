FROM openjdk:11.0.8-jre-slim
VOLUME /tmp
RUN apt update
RUN apt install -y curl
COPY target/scratch*-jar-with-dependencies.jar app.jar
# ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar --spring.config.location=file:/config/properties.yaml
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.config.location=file:/config/properties.yaml"]
EXPOSE 8080
