package st.bas;

import java.util.Map;

public class Main {
    public static void main(String... args) {
        System.out.println("Scratchpad started");

        for(String arg : args) {
            System.out.println("\tArg received in program: " + arg);
        }

        System.out.println("\n");

        final var getenv = System.getenv();
        for (var entry : getenv.entrySet()) {
            System.out.println("\tEnv received in program: key=" + entry.getKey() + ", value=" + entry.getValue());
        }
    }
}
